;;; simple-theme.el --- Simple Emacs theme

;; Copyright 2019 Alexey Abramov

;; Author: Alexey Abramov (string-append "levenson" "@" "mmer.org")
;; URL: https://gitlab.com/Levenson/emacs-simple-theme
;; Version: 0

;; This file is not part of GNU Emacs.

;; This file is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.

;; This file is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; Very light theme, which doesn't specify any particular light/dark
;; colors, but improves defaults.

;;; Code:

(deftheme simple
  "Created 2019-04-26.")

(defun simple/visual-bell ()
  (invert-face 'mode-line)
  (run-with-idle-timer 0.1 nil #'invert-face 'mode-line))

(custom-theme-set-variables
 'simple
 '(visible-bell nil)
 '(ring-bell-function 'simple/visual-bell))

(custom-theme-set-faces
 'simple

 ;; Base
 '(default ((t (:family "Hack"))))
 '(highlight ((t (:background "brown4"))))
 '(region ((t (:background "medium slate blue"))))

 '(mode-line ((t (:box (:line-width 6 :color "grey85") :foreground "black" :background "grey85"))))
 '(mode-line-highlight ((t (:box nil :inherit mode-line))))
 '(mode-line-inactive ((t (:inherit mode-line)))))

;; Automatically add this theme to the load path
;;;###autoload
(and load-file-name
    (boundp 'custom-theme-load-path)
    (add-to-list 'custom-theme-load-path
                 (file-name-as-directory
                  (file-name-directory load-file-name))))

(provide-theme 'simple)

;; Local Variables:
;; no-byte-compile: t
;; End:

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; simple-theme.el ends here
