;;; simple-dark-color-theme.el --- Simple Dark Emacs theme

;; Copyright 2019 Alexey Abramov

;; Author: Alexey Abramov (string-append "levenson" "@" "mmer.org")
;; URL: https://gitlab.com/Levenson/emacs-simple-theme
;; Version: 0

;; This file is not part of GNU Emacs.

;; This file is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.

;; This file is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; Dark colors for simple-theme.

(deftheme simple-dark-color)

(custom-theme-set-faces
 'simple-dark-color

 '(default ((t (:foreground "#F8F8F8" :background "#141414"))))
 '(fringe ((t (:background "#141414"))))
 '(cursor ((t (:foreground "#F07746" :background "#fce94f"))))

 '(highlight ((t (:background "Brown4"))))
 '(region ((t (:background "DarkSlateBlue" :distant-foreground "#827CAC"))))

 ;; Mode line
 '(mode-line ((t :box (:line-width 6 :color "Black") :foreground "#F8F8F8" :background "Black")))
 '(mode-line-highlight ((t :box nil)))
 '(mode-line-inactive ((t :box (:line-width 6 :color "#101010") :background "#101010")))

 ;; Minibuffer
 '(minibuffer-prompt ((t (:foreground "Cyan"))))

 '(font-lock-comment-face ((t (:foreground "#848484"))))
 '(font-lock-function-name-face ((t (:foreground "#729fcf"))))
 '(font-lock-keyword-face ((t (:foreground "#00bfff"))))

 '(message-header-name ((t (:foreground "Tomato"))))
 '(message-header-newsgroups ((t (:italic t :bold t :foreground "LightSkyBlue3"))))
 '(message-header-other ((t (:foreground "LightSkyBlue3"))))
 '(message-header-xheader ((t (:foreground "DodgerBlue3"))))
 '(message-header-subject ((t (:foreground "White"))))
 '(message-header-to ((t (:foreground "White"))))
 '(message-header-cc ((t (:foreground "White"))))

 ;; ace-window
 '(aw-leading-char-face ((t (:bold t :foreground "Yellow"))))
 '(aw-mode-line-face ((t (:bold t :foreground "Yellow"))))

 ;; Dired+
 '(diredp-dir-heading ((t (:foreground "#7474FFFFFFFF" :weight bold))))
 '(diredp-dir-name ((t (:foreground "#7474FFFFFFFF"))))
 '(diredp-file-name ((t (:inherit 'default))))
 '(diredp-dir-priv ((t (:foreground "#7474FFFFFFFF"))))
 '(diredp-no-priv ((t nil)))

 ;; Gnus
 '(gnus-cite-face-1 ((t (:foreground "#ad7fa8"))))
 '(gnus-cite-face-2 ((t (:foreground "sienna4"))))
 '(gnus-cite-face-3 ((t (:foreground "khaki4"))))
 '(gnus-cite-face-4 ((t (:foreground "PaleTurquoise4"))))
 '(gnus-group-mail-1-empty((t (:foreground "light cyan"))))
 '(gnus-group-mail-1((t (:bold t :foreground "light cyan"))))
 '(gnus-group-mail-2-empty((t (:foreground "turquoise"))))
 '(gnus-group-mail-2((t (:bold t :foreground "turquoise"))))
 '(gnus-group-mail-3-empty((t (:foreground "#729fcf"))))
 '(gnus-group-mail-3((t (:bold t :foreground "#edd400"))))
 '(gnus-group-mail-low-empty((t (:foreground "dodger blue"))))
 '(gnus-group-mail-low((t (:bold t :foreground "dodger blue"))))
 '(gnus-group-news-1-empty((t (:foreground "light cyan"))))
 '(gnus-group-news-1((t (:bold t :foreground "light cyan"))))
 '(gnus-group-news-2-empty((t (:foreground "turquoise"))))
 '(gnus-group-news-2((t (:bold t :foreground "turquoise"))))
 '(gnus-group-news-3-empty((t (:foreground "#729fcf"))))
 '(gnus-group-news-3((t (:bold t :foreground "#edd400"))))
 '(gnus-group-news-low-empty((t (:foreground "dodger blue"))))
 '(gnus-group-news-low((t (:bold t :foreground "dodger blue"))))
 '(gnus-header-name ((t (:bold t :foreground "#729fcf"))))
 '(gnus-header-from ((t (:bold t :foreground "#edd400"))))
 '(gnus-header-subject ((t (:foreground "#edd400"))))
 '(gnus-header-content ((t (:italic t :foreground "#8ae234"))))
 '(gnus-header-newsgroups((t (:italic t :bold t :foreground "LightSkyBlue3"))))
 '(gnus-signature((t (:italic t :foreground "dark grey"))))
 '(gnus-summary-cancelled((t (:background "Black" :foreground "yellow"))))
 '(gnus-summary-high-ancient((t (:bold t :foreground "royal blue"))))
 '(gnus-summary-high-read((t (:bold t :foreground "lime green"))))
 '(gnus-summary-high-ticked((t (:bold t :foreground "Tomato"))))
 '(gnus-summary-high-unread((t (:bold t :foreground "White"))))
 '(gnus-summary-low-ancient((t (:italic t :foreground "lime green"))))
 '(gnus-summary-low-read ((t (:foreground "#666666" :slant italic))))
 ;; '(gnus-summary-low-read((t (:italic t :foreground "royal blue"))))
 '(gnus-summary-low-ticked((t (:italic t :foreground "dark red"))))
 '(gnus-summary-low-unread((t (:italic t :foreground "White"))))
 '(gnus-summary-normal-ancient((t (:foreground "royal blue"))))
 '(gnus-summary-normal-read((t (:foreground "lime green"))))
 '(gnus-summary-normal-ticked((t (:foreground "indian red"))))
 '(gnus-summary-normal-unread((t (:foreground "White"))))
 '(gnus-summary-selected ((t (:background "Brown4" :foreground "White"))))

 '(helm-swoop-target-line-block-face ((t (:inherit highlight))))
 '(helm-swoop-target-line-face ((t (:inherit highlight))))
 '(helm-swoop-target-word-face ((t (:inherit isearch))))

 ;; Helm
 '(helm-buffer-directory ((t (:foreground "#7474FFFFFFFF"))))
 '(helm-selection ((t (:bold t :background "DarkSlateBlue"))))
 '(helm-source-header ((t (:background "Black" :box (:line-width 6 :color "Black") :weight bold)))))

;; Automatically add this theme to the load path
;;;###autoload
(and load-file-name
    (boundp 'custom-theme-load-path)
    (add-to-list 'custom-theme-load-path
                 (file-name-as-directory
                  (file-name-directory load-file-name))))



(provide-theme 'simple-dark-color)

;; Local Variables:
;; no-byte-compile: t
;; End:

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; simple-dark-color-theme.el ends here
